{
    // Set Dog Name
    $("#txt-dog-name").on('keydown keyup',function(){
        currentName = $(this).val();
        $("#dog-name").html(currentName)
    });

    // Change Color
    $("#slct-color").on('change',function(){
        currentColor = $(this).val();
        $("#dog-name").attr("data-color",currentColor)
    });

    // Change Font
    $("#slct-font").on('change',function(){
        currentFont = $(this).val();
        $("#dog-name").attr("data-font",currentFont)
    });

    // Animate Label
    $(".animated-label-field").each(function(){
        $(this).on('change keypress',function(){
        if($(this).val().length > 0)
            $(this).find('~ label').addClass('up-label');
        else
            $(this).find('~ label').removeClass('up-label');
        });
    });
}