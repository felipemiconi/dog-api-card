{
    var dogBreeds = "https://dog.ceo/api/breeds/list/all";

    // Dog Breeds
    $.getJSON(dogBreeds, function(data) {
        var obj = Object.keys(data.message);

        // Get Breeds
        Object.getOwnPropertyNames(obj).forEach(function(val) {
            // Set Breeds
            $('#dogs-list').append('<option value="'+ obj[val] +'">');
        });
    });
    
    // Dog's Selected Image
    $("#txt-dogs-list").on('change',function(){
        // Get Breeds
        var currentDog = $(this).val();
        var imgDogBreeds = "https://dog.ceo/api/breed/"+currentDog+"/images/random";

        
        $.getJSON(imgDogBreeds, function(data) {
            var obj = data.message;

            // Set Image
            $('#dog-random-img').attr('src', obj).attr('alt', currentDog);
        });
    });
}

