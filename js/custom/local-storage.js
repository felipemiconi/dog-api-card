$(function() {
    var currantDate = new Date();

    $("#btn-save").click(function() {
      var breeds = $("#txt-dogs-list");
      var name = $("#txt-dog-name");
      var color = $("#slct-color");
      var font = $("#slct-font");

      // Check if all field have content
      if (breeds.val().length == 0 || name.val().length == 0 || color.find("option:selected").val().length == 0 || font.find("option:selected").val().length == 0) {
        // Case False

        // Show warning
        $("#alert").removeClass();
        $("#alert").addClass("warning");
        $("#alert .text").html("Por favor, preencha todos os campos antes de salvar as informações.");
        showAlert();
      } else {
        // Case True

        // Save Datas on Local Storage
        localStorage.setItem('raça-cachorro', breeds.val());
        localStorage.setItem('nome-cachorro', name.val());
        localStorage.setItem('cor', color.val());
        localStorage.setItem('fonte', font.val());
        localStorage.DataHora = currantDate;

        // Run Alert
        $("#alert").removeClass();
        $("#alert").addClass("success");
        $("#alert .text").html("Informações Salvas com Sucesso!");
        showAlert();

        // Clear fields
        breeds.val("");
        name.val("");
        color.val("");
        font.val("");
      }
        
    });
  
    // Clear Local Storage Data
    // $("#btn-clear-data-storage").click(function() {
    //   localStorage.clear();
    //   window.location = window.location;
    // });
  });