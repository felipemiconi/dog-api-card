function showAlert() {
    $("#alert").fadeIn(
        function() {
            setTimeout(closeAlert,2000);
            function closeAlert() {
                $("#alert").fadeOut();
            };
        }
    );
}