function showAlert() {
    $("#alert").fadeIn(
        function() {
            setTimeout(closeAlert,2000);
            function closeAlert() {
                $("#alert").fadeOut();
            };
        }
    );
}
{
    var dogBreeds = "https://dog.ceo/api/breeds/list/all";

    // Dog Breeds
    $.getJSON(dogBreeds, function(data) {
        var obj = Object.keys(data.message);

        // Get Breeds
        Object.getOwnPropertyNames(obj).forEach(function(val) {
            // Set Breeds
            $('#dogs-list').append('<option value="'+ obj[val] +'">');
        });
    });
    
    // Dog's Selected Image
    $("#txt-dogs-list").on('change',function(){
        // Get Breeds
        var currentDog = $(this).val();
        var imgDogBreeds = "https://dog.ceo/api/breed/"+currentDog+"/images/random";

        
        $.getJSON(imgDogBreeds, function(data) {
            var obj = data.message;

            // Set Image
            $('#dog-random-img').attr('src', obj).attr('alt', currentDog);
        });
    });
}


{
    // Set Dog Name
    $("#txt-dog-name").on('keydown keyup',function(){
        currentName = $(this).val();
        $("#dog-name").html(currentName)
    });

    // Change Color
    $("#slct-color").on('change',function(){
        currentColor = $(this).val();
        $("#dog-name").attr("data-color",currentColor)
    });

    // Change Font
    $("#slct-font").on('change',function(){
        currentFont = $(this).val();
        $("#dog-name").attr("data-font",currentFont)
    });

    // Animate Label
    $(".animated-label-field").each(function(){
        $(this).on('change keypress',function(){
        if($(this).val().length > 0)
            $(this).find('~ label').addClass('up-label');
        else
            $(this).find('~ label').removeClass('up-label');
        });
    });
}
$(function() {
    var currantDate = new Date();

    $("#btn-save").click(function() {
      var breeds = $("#txt-dogs-list");
      var name = $("#txt-dog-name");
      var color = $("#slct-color");
      var font = $("#slct-font");

      // Check if all field have content
      if (breeds.val().length == 0 || name.val().length == 0 || color.find("option:selected").val().length == 0 || font.find("option:selected").val().length == 0) {
        // Case False

        // Show warning
        $("#alert").removeClass();
        $("#alert").addClass("warning");
        $("#alert .text").html("Por favor, preencha todos os campos antes de salvar as informações.");
        showAlert();
      } else {
        // Case True

        // Save Datas on Local Storage
        localStorage.setItem('raça-cachorro', breeds.val());
        localStorage.setItem('nome-cachorro', name.val());
        localStorage.setItem('cor', color.val());
        localStorage.setItem('fonte', font.val());
        localStorage.DataHora = currantDate;

        // Run Alert
        $("#alert").removeClass();
        $("#alert").addClass("success");
        $("#alert .text").html("Informações Salvas com Sucesso!");
        showAlert();

        // Clear fields
        breeds.val("");
        name.val("");
        color.val("");
        font.val("");
      }
        
    });
  
    // Clear Local Storage Data
    // $("#btn-clear-data-storage").click(function() {
    //   localStorage.clear();
    //   window.location = window.location;
    // });
  });