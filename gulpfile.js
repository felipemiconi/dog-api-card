// Add Modules
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');

// SASS Compiler + Set Prefix
function compilaSass() {
  return gulp
  .src('css/scss/**/*.scss')
  .pipe(sass({
    outputStyle: 'compressed'
  }))
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest('css/'))
  .pipe(browserSync.stream());
}
// SASS Task
gulp.task('sass', compilaSass);

// Minify JS
function gulpJS() {
  return gulp
  .src('js/custom/**/*.js')
  .pipe(concat('main.js'))
  .pipe(gulp.dest('js/'))
}
// JS Task
gulp.task('mainjs', gulpJS);

// Start Browser
function browser() {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
}
// Browser Task
gulp.task('browser-sync', browser);

// Watch Gulp's Function
function watch() {
  gulp.watch('css/scss/**/*.scss', compilaSass);
  gulp.watch('js/custom/**/*.js', gulpJS);
  gulp.watch(['*.html']).on('change', browserSync.reload);
}
// Watch Task
gulp.task('watch', watch);

// Run All Tasks
gulp.task('default', gulp.parallel('watch', 'browser-sync', 'sass', 'mainjs'));